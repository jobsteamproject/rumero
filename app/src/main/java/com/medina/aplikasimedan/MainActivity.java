package com.medina.aplikasimedan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_kuliner = findViewById(R.id.kuliner);
        btn_kuliner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, KulinerActivity.class));
                finish();
            }
        });

        Button btn_wisata = findViewById(R.id.wisata);
        btn_wisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, WisataActivity.class));
                finish();
            }
        });

        Button btn_bioskop = findViewById(R.id.bioskop);
        btn_bioskop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, BioskopActivity.class));
                finish();
            }
        });

        Button btn_souvenir = findViewById(R.id.souvenir);
        btn_souvenir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SouvenirActivity.class));
                finish();
            }
        });
    }
}
