package com.medina.aplikasimedan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    public static String id, namaToko,alamatToko,teleponToko,bukaToko,deskripsiToko,gambarToko;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar mActionBar = (Toolbar) findViewById(R.id.toolbar);
        mActionBar.setTitle("Detail Info");
        mActionBar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mActionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //What to do on back clicked

                startActivity(new Intent(DetailActivity.this, MainActivity.class));
                finish();
            }
        });

        TextView nama_toko = findViewById(R.id.Nama);
        nama_toko.setText(namaToko);

        TextView alamat_toko = findViewById(R.id.Alamat);
        alamat_toko.setText(alamatToko);

        TextView telepon_toko = findViewById(R.id.Telepon);
        telepon_toko.setText(teleponToko);

        TextView buka_toko = findViewById(R.id.Buka);
        buka_toko.setText(bukaToko);

        TextView deskripsi_toko = findViewById(R.id.Deskripsi);
        deskripsi_toko.setText(deskripsiToko);


        ImageView gambar_toko = findViewById(R.id.Gambar);
        Picasso.get().load(gambarToko).into(gambar_toko);

    }
}
