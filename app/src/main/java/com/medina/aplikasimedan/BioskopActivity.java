package com.medina.aplikasimedan;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BioskopActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private  String[] id,nama,alamat,telepon,buka,deskripsi,gambar;
    int numData;
    LatLng latLng[];
    Boolean markerD[];
    private Double[] latitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bioskop);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getLokasi();

        Toolbar mActionBar = (Toolbar) findViewById(R.id.toolbar);
        mActionBar.setTitle("Bioskop");
        mActionBar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        mActionBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //What to do on back clicked

                startActivity(new Intent(BioskopActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    private void getLokasi() {
        String url = "http://unlimitenergy.com/json_bioskop.php";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //deklarasi DATA JSON Object
                numData = response.length();
                Log.d("DEBUG_", "Parse JSON");
                latLng = new LatLng[numData];
                markerD = new Boolean[numData];
                id = new String[numData];
                nama = new String[numData];
                alamat = new String[numData];
                telepon = new String[numData];
                buka = new String[numData];
                deskripsi = new String[numData];
                gambar = new String[numData];
                latitude = new Double[numData];
                longitude = new Double[numData];

                //melakukan perulangan untuk mendapatkan semua value dari JSON
                for (int i = 0; i < numData; i++) {
                    try {
                        // mendapatkan value dari JSON
                        JSONObject data = response.getJSONObject(i);
                        id[i] = data.getString("id");
                        latLng[i] = new LatLng(data.getDouble("latitude"),data.getDouble("longitude"));
                        nama[i] = data.getString("nama");
                        alamat[i] = data.getString("alamat");
                        telepon[i] = data.getString("telepon");
                        buka[i] = data.getString("buka");
                        deskripsi[i] = data.getString("deskripsi");
                        gambar[i] = data.getString("gambar");
                        latitude[i] = data.getDouble("latitude");
                        longitude[i] = data.getDouble("longitude");

                        //membuat marker point
                        markerD[i] = false;
                        mMap.addMarker(new MarkerOptions()
                                .position(latLng[i])    //posisi
                                .title(nama[i])         //judul
                                .snippet(alamat[i])     //alamat
                                .icon(BitmapDescriptorFactory.fromResource(
                                        R.drawable.marker))); //gambar marker

                    } catch (JSONException je) {
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng[i], 12.5f)); //posisi camera di map
                }

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener(){
                    @Override
                    public boolean onMarkerClick(Marker marker){
                        Log.d("DEBUG_", "Marker Clicked");
                        for(int i = 0; i < numData; i++){
                            if(marker.getTitle().equals(nama[i])){
                                if(markerD[i]){
                                    Log.d("DEBUG_", "Panggil aktivity");
                                    DetailActivity.id = id[i];
                                    DetailActivity.namaToko = nama[i];
                                    DetailActivity.alamatToko = alamat[i];
                                    DetailActivity.bukaToko = buka[i];
                                    DetailActivity.teleponToko = telepon[i];
                                    DetailActivity.gambarToko = gambar[i];
                                    DetailActivity.deskripsiToko = deskripsi[i];

                                    Intent intent = new Intent(BioskopActivity.this, DetailActivity.class);
                                    startActivity(intent);
                                    markerD[i] = false;
                                }else {
                                    Log.d("DEBUG_","Show Info");
                                    // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15.5f));
                                    markerD[i] = true;
                                    marker.showInfoWindow();
                                    Toast ts = Toast.makeText(BioskopActivity.this,"Tap Sekali Lagi, Untuk Melihat Detail",Toast.LENGTH_LONG);
                                    TextView v = (TextView) ts.getView().findViewById(android.R.id.message);
                                    if( v != null)
                                        v.setGravity(Gravity.CENTER);
                                    ts.show();
                                }
                            }else {
                                markerD[i] = false;
                            }
                        }
                        return false;
                    }
                });

            }
        }, new Response.ErrorListener() { //error ketika response gagal
            @Override
            public void onErrorResponse(VolleyError error) {
                //menampilkan alert dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(BioskopActivity.this);
                builder.setTitle("Error!");
                builder.setMessage("Tidak Ada Koneksi Internet");
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getLokasi();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        Volley.newRequestQueue(this).add(request);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true); //button gps
        mMap.getUiSettings().setZoomControlsEnabled(true); //button zoomIN/OUT
        mMap.getUiSettings().setCompassEnabled(true); //simbol compas
        mMap.getUiSettings().setMapToolbarEnabled(true); //map toolbar
    }
}
